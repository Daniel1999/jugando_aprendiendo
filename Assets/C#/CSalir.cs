﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSalir : MonoBehaviour
{
    public void Salir()
    {
        Application.Quit();
        Debug.Log("Se ha salido del juego");
    }
}
